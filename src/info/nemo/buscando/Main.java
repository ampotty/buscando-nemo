package info.nemo.buscando;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        int opcion;
        float p_exito;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Pez> cooler = new ArrayList<>();

        while (true) {
            System.out.println("\nBUSCANDO A NEMO");
            System.out.println("                 |\n" +
                    "                 |\n" +
                    "                ,|.\n" +
                    "               ,\\|/.\n" +
                    "             ,' .V. `.\n" +
                    "            / .     . \\\n" +
                    "           /_`       '_\\\n" +
                    "          ,' .:     ;, `.\n" +
                    "          |@)|  . .  |(@|\n" +
                    "     ,-._ `._';  .  :`_,' _,-.\n" +
                    "    '--  `-\\ /,-===-.\\ /-'  --`\n" +
                    "   (----  _|  ||___||  |_  ----)\n" +
                    "    `._,-'  \\  `-.-'  /  `-._,'\n" +
                    "             `-.___,-' ");
            System.out.println(" 1. Pescar");
            System.out.println(" 2. Ver cooler");
            System.out.println(" 3. Salir");
            System.out.print("\nOpcion > ");
            try {
                opcion = Integer.parseInt(br.readLine());
            } catch (Exception e) {
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    System.out.println("\nVamos a pescar...");
                    p_exito = (float) (Math.random()*101);
                    if (p_exito < 40.0) {
                        System.out.println("Tas en panga!");
                    } else {
                        Pez p = new Pez();
                        cooler.add(p);
                        System.out.println("Agarra tu pez!");
                    }
                    break;
                case 2:
                    System.out.println("\nRevisando el cooler...");
                    System.out.println(String.format("Tenemos %d peces", cooler.size()));
                    for (Pez pez : cooler) {
                        System.out.println(String.format("%s de %.2f libras", pez.getEspecie(), pez.getPeso()));
                        if (pez.getEspecie() == "NEMO") {
                            System.out.println("Eres venao...");
                            System.exit(0);
                        }
                    }
                    break;
                case 3:
                    System.exit(0);
                    break;
                default:
                    System.out.println("\nError::Opcion Invalida");
            }
        }
    }
}
